﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using tallyService.Models;
using tallyService.Repository;

namespace tallyService.Controllers
{
    public abstract class AbstractController : Controller
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;

        public AbstractController(ILogger logger, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
        }

        public abstract string Uri { get; }

        public abstract string BadRequestMessage { get; }

        public abstract string NotFoundMessage { get; }

        public abstract string GetRecordFailedMessage { get; }

        public abstract string GetNewRecordUri<T>(T newEntry);

        public abstract string DeleteRecordMessage<T>(T deletedEntry, bool isDeleted);

        public IActionResult GetRecord<T1, T2>(IBasicActions<T2> action, int id)
        {
            try
            {
                var record = action.GetRecord(id);

                if (record != null) return Ok(_mapper.Map<T2, T1>(record.Result));

                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"Failed to get {id} record. Exception : {ex}");
                return BadRequest(String.Format(GetRecordFailedMessage, id));
            }
        }

        public IActionResult AddRecord<T1, T2>(IBasicActions<T2> action, T1 entry)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    T2 newEntry = _mapper.Map<T1, T2>(entry);
                    action.AddRecord(newEntry);

                    T1 entryToReturn = _mapper.Map<T2, T1>(newEntry);
                    return Created(GetNewRecordUri(entryToReturn), entryToReturn);
                }
                else
                {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"Failed to add record. Exception : {ex}");
            }

            return BadRequest(BadRequestMessage);
        }

        public async Task<IActionResult> UpdateRecord<T1, T2>(IBasicActions<T2> action, int id, T1 updatedEntry)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    T2 existingEntry = await action.GetRecord(id, false);
                    if (updatedEntry == null) { return NotFound(NotFoundMessage); }

                    _mapper.Map<T1, T2>(updatedEntry, existingEntry);
                    bool result = action.UpdateRecord(existingEntry);
                    if (result) {
                        T1 accountToReturn = _mapper.Map<T2, T1>(existingEntry);
                        return Created(GetNewRecordUri(accountToReturn), accountToReturn);
                    }
                }
                else {
                    return BadRequest(ModelState);
                }
            }
            catch (Exception ex) {
                _logger.LogWarning($"Failed to update record {id}. Exception : {ex}");
            }
            return BadRequest(BadRequestMessage);
        }

        public IActionResult DeleteRecord<T1>(IBasicActions<T1> action, int id)
        {
            try
            {
                var entry = action.GetRecord(id);
                if (entry != null)
                {
                    T1 record = entry.Result;
                    bool result = false;
                    if (record != null)
                    {
                        result = action.DeleteRecord(record);
                    }
                    if (result)
                        return Ok(DeleteRecordMessage(record, true));
                    else return BadRequest(DeleteRecordMessage(record, false));
                }

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogWarning($"Failed to delete {id} record. Exception : {ex}");
                return BadRequest(BadRequestMessage);
            }
        }
    }
}
