﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tallyService.Repository;

namespace tallyService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[Controller]")]
    public class AccountSummaryController : Controller
    {
        private readonly ITallyRepository _repository;
        private readonly ILogger<AccountsController> _logger;

        public AccountSummaryController(ITallyRepository repository, ILogger<AccountsController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        // GET: api/AccountSummary
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/AccountSummary/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            try
            {
                var balance = _repository.Accounts.GetAccountBalance(id);
                return Ok(balance);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get account balance.");
                return BadRequest("Failed to get account balance.");
            }
        }
        
        //// POST: api/AccountSummary
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}
        
        //// PUT: api/AccountSummary/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}
        
        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
