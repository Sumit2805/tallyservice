﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tallyService.Models;
using tallyService.Repository;
using tallyService.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace tallyService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/accounts/{accountID}/transactions")]
    public class AccountTransactionController : Controller
    {
        private readonly ITallyRepository _repository;
        private readonly ILogger<AccountsController> _logger;
        private readonly IMapper _mapper;

        public AccountTransactionController(ITallyRepository repository, 
                                            ILogger<AccountsController> logger, IMapper mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int accountID, bool? isDebit = null)
        {
            var transactions = await _repository.Transactions.GetTransactionByAccountId(accountID, isDebit);
            if (transactions != null) return Ok(_mapper.Map<List<Transactions>, List<TransactionsViewModel>>(transactions));

            return NotFound();
        }

        [HttpGet()]
        public async Task<IActionResult> GetByDate(int accountID, string startDate, string endDate, bool? isDebit = null)
        {
            var transactions = await _repository.Transactions.GetTransactionByDate(
                                       DateTime.Parse(startDate), DateTime.Parse(endDate), isDebit);
            if (transactions != null) return Ok(_mapper.Map<List<Transactions>, List<TransactionsViewModel>>(transactions));

            return NotFound();
        }
    }
}
