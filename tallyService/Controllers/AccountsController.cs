﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tallyService.Models;
using tallyService.Repository;
using tallyService.ViewModels;

namespace tallyService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[Controller]")]
    public class AccountsController : AbstractController
    {
        private readonly ITallyRepository _repository;
        private readonly ILogger<AccountsController> _logger;
        private readonly IMapper _mapper;

        public AccountsController(ITallyRepository repository, ILogger<AccountsController> logger, IMapper mapper) : base (logger, mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        public override string Uri => $"/api/accounts/";

        public override string BadRequestMessage => "Failed to add/update/delete account request.";

        public override string NotFoundMessage => "Account not found.";

        public override string GetRecordFailedMessage => "Failed to get account {0}.";

        public override string GetNewRecordUri<T>(T newEntry)
        {
            AccountViewModel ac = newEntry as AccountViewModel;
            return Uri + ac.AccountId;
        }

        private static string accountDeleted = "Account '{0}' deleted successfully.";
        private static string accountNotDeleted = "Failed to delete account '{0}'.";
        public override string DeleteRecordMessage<T>(T deletedEntry, bool isDeleted)
        {
            Accounts ac = deletedEntry as Accounts;
            return String.Format((isDeleted? accountDeleted : accountNotDeleted), ac.Name);
        }

        [HttpGet()]
        public IActionResult Get()
        {
            try
            {
                bool all = true;
                return Ok(
                    _mapper.Map<List<Accounts>, List<AccountViewModel>>
                                (_repository.Accounts.GetAccounts(all)));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get accounts.");
                return BadRequest("Failed to get accounts.");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetAccount(int id)
        {
            return base.GetRecord<AccountViewModel, Accounts>(_repository.Accounts as IBasicActions<Accounts>, id);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult CreateAccount([FromBody]AccountViewModel accountVM)
        {
            return base.AddRecord<AccountViewModel, Accounts>(_repository.Accounts as IBasicActions<Accounts>, accountVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public Task<IActionResult> UpdateAccount([FromBody]AccountViewModel accountVM)
        {
            return base.UpdateRecord<AccountViewModel, Accounts>(_repository.Accounts as IBasicActions<Accounts>, accountVM.AccountId, accountVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult DeleteAccount(int id)
        {
            return base.DeleteRecord<Accounts>(_repository.Accounts as IBasicActions<Accounts>, id);
        }
    }
}