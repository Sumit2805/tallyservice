﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tallyService.Models;
using tallyService.Repository;
using tallyService.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace tallyService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[Controller]")]
    public class CategoryController : AbstractController
    {
        private readonly ITallyRepository _repository;
        private readonly ILogger<CategoryController> _logger;
        private readonly IMapper _mapper;

        public CategoryController(ITallyRepository repository, ILogger<CategoryController> logger, IMapper mapper) : base(logger, mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        public override string Uri => "/api/category/";

        public override string BadRequestMessage => "Failed to add/update/delete category request.";

        public override string NotFoundMessage => "Category not found.";

        public override string GetRecordFailedMessage => "Failed to get category {0}.";

        public override string GetNewRecordUri<T>(T newEntry)
        {
            CategoryViewModel tn = newEntry as CategoryViewModel;
            return Uri + tn.Id;
        }

        private static string transactionDeleted = "Category '{0}' deleted successfully.";
        private static string transactionNotDeleted = "Failed to delete category '{0}'.";
        public override string DeleteRecordMessage<T>(T deletedEntry, bool isDeleted)
        {
            Category ac = deletedEntry as Category;
            return String.Format((isDeleted ? transactionDeleted : transactionNotDeleted), ac.Id);
        }

        [HttpGet()]
        public async Task<IActionResult> Get(bool? isIncome = null)
        {
            try
            {
                return Ok(
                    _mapper.Map<List<Category>, List<CategoryViewModel>>
                                (await _repository.Category.GetCategories(isIncome)));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get categories. isIncome = {0}", isIncome);
                return BadRequest("Failed to get categories.");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetCategory(int id)
        {
            return base.GetRecord<CategoryViewModel, Category>(_repository.Category as IBasicActions<Category>, id);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult CreateCategory([FromBody]CategoryViewModel categoryVM)
        {
            return base.AddRecord<CategoryViewModel, Category>(_repository.Category as IBasicActions<Category>, categoryVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public Task<IActionResult> UpdateCategory([FromBody]CategoryViewModel categoryVM)
        {
            return base.UpdateRecord<CategoryViewModel, Category>(_repository.Category as IBasicActions<Category>, categoryVM.Id, categoryVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult DeleteCategory(int id)
        {
            return base.DeleteRecord<Category>(_repository.Category as IBasicActions<Category>, id);
        }
    }
}
