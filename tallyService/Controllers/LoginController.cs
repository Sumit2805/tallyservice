﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using tallyService.ViewModels;

namespace tallyService.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        string privateSecretKey = String.Empty;

        public LoginController(IConfiguration configuration)
        {
            privateSecretKey = configuration["PrivateSecretKey"];
        }

        [AllowAnonymous]
        [HttpPost]
        public IActionResult RequestToken([FromBody] LoginRequestViewModel request)
        {
            UserService userService = new UserService();
            string role = userService.IsValidUser(request.Username, request.Password);
            if (!String.IsNullOrEmpty(role))
            {
                var claims = new List<Claim>
                {
                     new Claim(ClaimTypes.Name, request.Username),
                     new Claim(ClaimTypes.Role, role)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(privateSecretKey));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "tally.com",
                    audience: "tally.com",
                    claims: claims.ToArray(),
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: creds);

                return Ok(new
                {
                    accessright = role,
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }

            return BadRequest("Could not verify username and password");
        }
    }
}
