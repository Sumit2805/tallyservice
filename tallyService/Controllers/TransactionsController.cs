﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tallyService.Models;
using tallyService.Repository;
using tallyService.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace tallyService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[Controller]")]
    public class TransactionsController : AbstractController
    {
        private readonly ITallyRepository _repository;
        private readonly ILogger<AccountsController> _logger;
        private readonly IMapper _mapper;

        public override string Uri => "/api/transactions/";

        public override string BadRequestMessage => "Failed to add/update/delete transaction request.";

        public override string NotFoundMessage => "Transaction not found.";

        public override string GetRecordFailedMessage => "Failed to get transaction {0}.";

        public override string GetNewRecordUri<T>(T newEntry)
        {
            TransactionsViewModel tn = newEntry as TransactionsViewModel;
            return Uri + tn.TransactionId;
        }

        private static string transactionDeleted = "Transaction '{0}' deleted successfully.";
        private static string transactionNotDeleted = "Failed to delete transaction '{0}'.";
        public override string DeleteRecordMessage<T>(T deletedEntry, bool isDeleted)
        {
            Transactions ac = deletedEntry as Transactions;
            return String.Format((isDeleted ? transactionDeleted : transactionNotDeleted), ac.Id);
        }

        public TransactionsController(ITallyRepository repository, ILogger<AccountsController> logger, IMapper mapper) : base(logger, mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet()]
        public async Task<IActionResult> GetByDate(string startDate, string endDate, bool? isDebit = null)
        {
            try
            {
                var transactions = await _repository.Transactions.GetTransactionByDate(
                                           DateTime.Parse(startDate), DateTime.Parse(endDate), isDebit);
                if (transactions != null) return Ok(_mapper.Map<List<Transactions>, List<TransactionsViewModel>>(transactions));

                return NotFound();
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Failed to get transactions. StartDate={0},endDate={1},isDebit", startDate, endDate, isDebit);
                _logger.LogWarning("Failed to get transactions.");
                return BadRequest("Failed to get transactions.");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            return base.GetRecord<TransactionsViewModel, Transactions>(_repository.Transactions as IBasicActions<Transactions>, id);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public IActionResult CreateTransaction([FromBody]TransactionsViewModel transactionVM)
        {
            //Correct some params
            transactionVM = AutoFillTransactionData(transactionVM);
            return base.AddRecord<TransactionsViewModel, Transactions>(_repository.Transactions as IBasicActions<Transactions>, transactionVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public Task<IActionResult> UpdateTransactions([FromBody]TransactionsViewModel transactionVM)
        {
            //Correct some params
            transactionVM = AutoFillTransactionData(transactionVM);
            return base.UpdateRecord<TransactionsViewModel, Transactions>(_repository.Transactions as IBasicActions<Transactions>, transactionVM.TransactionId, transactionVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult DeleteTransaction(int id)
        {
            return base.DeleteRecord<Transactions>(_repository.Transactions as IBasicActions<Transactions>, id);
        }

        private TransactionsViewModel AutoFillTransactionData(TransactionsViewModel transactionVM)
        {
            if (!String.IsNullOrEmpty(transactionVM.TransactionDate))
            {
                transactionVM.TransactionMonth = transactionVM.TransactionDate.Substring(0, 4) +
                                                 transactionVM.TransactionDate.Substring(5, 2);
            }
            return transactionVM;
        }
    }
}