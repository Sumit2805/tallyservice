﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using tallyService.Models;
using tallyService.Repository;
using tallyService.ViewModels;

namespace tallyService.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[Controller]")]
    public class TransfersController : AbstractController
    {
        private readonly ITallyRepository _repository;
        private readonly ILogger<TransfersController> _logger;
        private readonly IMapper _mapper;

        public override string Uri => $"/api/transfers/";

        public override string BadRequestMessage => "Failed to add/update/delete transfer request.";

        public override string NotFoundMessage => "Transfer not found.";

        public override string GetRecordFailedMessage => "Failed to get transfer {0}.";

        public override string GetNewRecordUri<T>(T newEntry)
        {
            TransferViewModel ac = newEntry as TransferViewModel;
            return Uri + ac.Id;
        }

        private static string accountDeleted = "Transfer '{0}' deleted successfully.";
        private static string accountNotDeleted = "Failed to delete transfer '{0}'.";
        public override string DeleteRecordMessage<T>(T deletedEntry, bool isDeleted)
        {
            Transfers ac = deletedEntry as Transfers;
            return String.Format((isDeleted ? accountDeleted : accountNotDeleted), ac.Id);
        }

        public TransfersController(ITallyRepository repository, ILogger<TransfersController> logger, IMapper mapper) : base(logger, mapper)
        {
            _repository = repository;
            _logger = logger;
            _mapper = mapper;
        }

        [HttpGet("from={fromID}")]
        public async Task<IActionResult> GetTransfersByFromId(int fromID)
        {
            try
            {
                return Ok(await _repository.Transfers.GetTransfersByFromId(fromID));
            }
            catch(Exception ex)
            {
                _logger.LogError($"Failed to get {fromID} account transfers. Exception : {ex}");
                return BadRequest("Failed to get transfers");
            }
        }

        [HttpGet("to={fromID}")]
        public async Task<IActionResult> GetTransfersByToId(int fromID)
        {
            try
            {
                return Ok(await _repository.Transfers.GetTransfersByToId(fromID));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Failed to get {fromID} account transfers. Exception : {ex}");
                return BadRequest("Failed to get transfers");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetTransfer(int id)
        {
            return base.GetRecord<TransferViewModel, Transfers>(_repository.Transfers as IBasicActions<Transfers>, id);
        }

        [HttpGet()]
        [HttpGet("from={fromId}")]
        [HttpGet("to={toId}")]
        public async Task<IActionResult> GetByDate(int? fromId, int? toId, string startDate, string endDate)
        {
            try
            {
                var transfers = await _repository.Transfers.GetTransfers(fromId, toId,
                                           DateTime.Parse(startDate), DateTime.Parse(endDate));
                if (transfers != null) return Ok(_mapper.Map<List<Transfers>, List<TransferViewModel>>(transfers));

                return NotFound();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Failed to get transfers. StartDate={0},endDate={1}", startDate, endDate);
                return BadRequest("Failed to get transactions.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("new")]
        public IActionResult CreateTransfer([FromBody]TransferViewModel transferVM)
        {
            transferVM.IsCleared = true;
            if (transferVM.FromId == null) return BadRequest("From account is null.");
            if (transferVM.ToId == null) return BadRequest("To account is null.");

            if (transferVM.FromId == transferVM.ToId)
            {
                return BadRequest("From/To account is same.");
            }
            return base.AddRecord<TransferViewModel, Transfers>(_repository.Transfers as IBasicActions<Transfers>, transferVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public Task<IActionResult> UpdateTransfer([FromBody]TransferViewModel transferVM)
        {
            return base.UpdateRecord<TransferViewModel, Transfers>(_repository.Transfers as IBasicActions<Transfers>, transferVM.Id, transferVM);
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public IActionResult DeleteTransfer(int id)
        {
            return base.DeleteRecord<Transfers>(_repository.Transfers as IBasicActions<Transfers>, id);
        }
    }
}
