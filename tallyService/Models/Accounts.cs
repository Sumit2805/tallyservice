﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace tallyService.Models
{
    public partial class Accounts
    {
        public Accounts()
        {
            Transactions = new HashSet<Transactions>();
            TransfersFrom = new HashSet<Transfers>();
            TransfersTo = new HashSet<Transfers>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public double Initial { get; set; }
        public int? Sequence { get; set; }
        public bool IsClosed { get; set; }
        public string Color { get; set; }
        public bool IsCredit { get; set; }
        public double? MinLimit { get; set; }

        public ICollection<Transactions> Transactions { get; set; }
        public ICollection<Transfers> TransfersFrom { get; set; }
        public ICollection<Transfers> TransfersTo { get; set; }
    }
}
