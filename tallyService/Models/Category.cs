﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace tallyService.Models
{
    public partial class Category
    {
        public Category()
        {
            Transactions = new HashSet<Transactions>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Color { get; set; }
        public bool IsIncome { get; set; }
        public int? Icon { get; set; }

        public ICollection<Transactions> Transactions { get; set; }
    }
}
