﻿using System;
using System.Collections.Generic;

namespace tallyService.Models
{
    public partial class Transactions
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public int CategoryId { get; set; }
        public int AccountId { get; set; }
        public string PayeeName { get; set; }
        public DateTime TrasactionDate { get; set; }
        public string TransactionMonth { get; set; }
        public bool? IsDebit { get; set; }
        public string Note { get; set; }
        public bool? IsRepeat { get; set; }
        public bool? Freq { get; set; }
        public int? Cycle { get; set; }
        public bool? IsCleared { get; set; }

        public Accounts Account { get; set; }
        public Category Category { get; set; }
    }
}
