﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace tallyService.Models
{
    public partial class Transfers
    {
        public int Id { get; set; }
        public int FromId { get; set; }
        public int ToId { get; set; }
        public double? Amount { get; set; }
        public DateTime TransferDate { get; set; }
        public string Note { get; set; }
        public bool? IsCleared { get; set; }

        public Accounts From { get; set; }
        public Accounts To { get; set; }
    }
}
