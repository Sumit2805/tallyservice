﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace tallyService.Models
{
    public partial class tallyDBContext : DbContext
    {
        public tallyDBContext()
        {
        }

        public tallyDBContext(DbContextOptions<tallyDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Transactions> Transactions { get; set; }
        public virtual DbSet<Transfers> Transfers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Accounts>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Color).HasColumnType("text");

                entity.Property(e => e.IsClosed).HasColumnName("Is_Closed");

                entity.Property(e => e.IsCredit).HasColumnName("Is_Credit");

                entity.Property(e => e.MinLimit).HasColumnName("Min_Limit");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Color).HasColumnType("text");

                entity.Property(e => e.IsIncome).HasColumnName("Is_Income");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<Transactions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AccountId).HasColumnName("Account_ID");

                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");

                entity.Property(e => e.IsCleared).HasColumnName("Is_Cleared");

                entity.Property(e => e.IsDebit).HasColumnName("Is_Debit");

                entity.Property(e => e.IsRepeat).HasColumnName("Is_Repeat");

                entity.Property(e => e.Note).HasColumnType("text");

                entity.Property(e => e.PayeeName)
                    .HasColumnName("Payee_Name")
                    .HasColumnType("text");

                entity.Property(e => e.TransactionMonth)
                    .IsRequired()
                    .HasColumnName("Transaction_Month")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.TrasactionDate)
                    .HasColumnName("Trasaction_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Trasnactions_Account_ID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Transfers_Category_ID");
            });

            modelBuilder.Entity<Transfers>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.FromId).HasColumnName("From_ID");

                entity.Property(e => e.IsCleared).HasColumnName("Is_Cleared");

                entity.Property(e => e.Note).HasColumnType("text");

                entity.Property(e => e.ToId).HasColumnName("To_ID");

                entity.Property(e => e.TransferDate)
                    .HasColumnName("Transfer_Date")
                    .HasColumnType("datetime");

                entity.HasOne(d => d.From)
                    .WithMany(p => p.TransfersFrom)
                    .HasForeignKey(d => d.FromId)
                    .HasConstraintName("FK_Transfers_From_ID_Account_ID");

                entity.HasOne(d => d.To)
                    .WithMany(p => p.TransfersTo)
                    .HasForeignKey(d => d.ToId)
                    .HasConstraintName("FK_Transfers_To_ID_Account_ID");
            });
        }
    }
}
