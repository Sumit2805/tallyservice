﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public class AccountRepo : IAccount
    {
        private readonly tallyDBContext _context;

        public AccountRepo(tallyDBContext context)
        {
            _context = context;
        }

        public List<Accounts> GetAccounts(bool includingClosed = false)
        {
            if (includingClosed)
            {
                return _context.Accounts.OrderBy(a => a.IsClosed).AsNoTracking().ToList();
            }
            else
            {
                return _context.Accounts.Where(a => a.IsClosed == false)
                    .OrderBy(a => a.Id).AsNoTracking().ToList();
            }
        }

        public async Task<Accounts> GetRecord(int accountID, bool trackingEnabled = false)
        {
            if (trackingEnabled)
            {
                return await _context.Accounts.Where(a => a.Id == accountID).FirstOrDefaultAsync();
            }
            
            return await _context.Accounts.AsNoTracking().Where(a => a.Id == accountID)
                .FirstOrDefaultAsync();
        }

        public Accounts AddRecord(Accounts account)
        {
            _context.Accounts.Add(account);
            _context.SaveChanges();

            return account;
        }

        public bool UpdateRecord(Accounts account)
        {
            _context.Accounts.Update(account);
            return _context.SaveChanges() > 0;
        }

        public bool DeleteRecord(Accounts accountToDelete)
        {
            var result = _context.Accounts.Remove(accountToDelete);
            return _context.SaveChanges() > 0;
        }

        public double GetAccountBalance(int accountID)
        {
            double balance = 0;
            Accounts account = GetRecord(accountID).Result;
            if (account != null)
            {
                double initialBalance = account.Initial;
                // Debit/Credit Amounts
                double transactionAmount = GetSumOfTransactionAmount(accountID);
                // Transfers
                double transferAmount = GetSumOfTransferAmount(accountID);
                balance = initialBalance + transactionAmount + transferAmount;
            }
            return balance;
        }

        private double GetSumOfTransactionAmount(int accountID)
        {
            double amount = 0;
            double debitAmount = 0;
            double creditAmount = 0;
            var transactions = _context.Transactions.Where(t => t.AccountId == accountID && t.IsDebit == false);
            if (transactions != null && transactions.Count() > 0)
            {
                debitAmount = transactions.Sum(t => t.Amount);
            }
            transactions = _context.Transactions.Where(t => t.AccountId == accountID && t.IsDebit == true);
            if (transactions != null && transactions.Count() > 0)
            {
                creditAmount = transactions.Sum(t => t.Amount);
            }
            amount = creditAmount - debitAmount;
            return amount;
        }

        private double GetSumOfTransferAmount(int accountID)
        {
            double amount = 0;
            double inAmount = 0;
            double outAmount = 0;
            var transferIn = _context.Transfers.Where(t => t.ToId == accountID);
            if (transferIn != null && transferIn.Count() > 0)
            {
                inAmount = transferIn.Sum(t => t.Amount).Value;
            }
            var transferOut = _context.Transfers.Where(t => t.FromId == accountID);
            if (transferOut != null && transferOut.Count() > 0)
            {
                outAmount = transferOut.Sum(t => t.Amount).Value;
            }
            amount = inAmount - outAmount;
            return amount;
        }
    }
}
