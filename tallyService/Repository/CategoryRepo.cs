﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public class CategoryRepo : ICategory
    {
        private readonly tallyDBContext _context;

        public CategoryRepo(tallyDBContext context)
        {
            _context = context;
        }

        public Category AddRecord(Category record)
        {
            _context.Category.Add(record);
            _context.SaveChanges();
            return record;
        }

        public bool DeleteRecord(Category record)
        {
            var result = _context.Category.Remove(record);
            return _context.SaveChanges() > 0;
        }

        public bool UpdateRecord(Category record)
        {
            _context.Category.Update(record);
            return _context.SaveChanges() > 0;
        }

        public async Task<Category> GetRecord(int id, bool trackingEnabled = false)
        {
            if (trackingEnabled)
            {
                return await _context.Category.Where(a => a.Id == id).FirstOrDefaultAsync();
            }
            return await _context.Category.AsNoTracking().Where(a => a.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<Category>> GetCategories(bool? isIncome = null)
        {
            if (isIncome.HasValue)
            {
                return await _context.Category.AsNoTracking().Where(c => c.IsIncome == isIncome.Value).ToListAsync();
            }
            else
            {
                return await _context.Category.AsNoTracking().ToListAsync();
            }
        }
    }
}
