﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public interface IAccount : IBasicActions<Accounts>
    {
        List<Accounts> GetAccounts(bool includingClosed = false);

        double GetAccountBalance(int accountId);
    }
}
