﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tallyService.Repository
{
    public interface IBasicActions<T>
    {
        T AddRecord(T record);
        bool UpdateRecord(T record);
        bool DeleteRecord(T record);

        Task<T> GetRecord(int id, bool trackingEnabled = false);
    }
}
