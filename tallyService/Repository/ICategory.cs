﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public interface ICategory : IBasicActions<Category>
    {
        Task<List<Category>> GetCategories(bool? isIncome = null);
    }
}
