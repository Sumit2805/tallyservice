﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tallyService.Repository
{
    public interface ITallyRepository
    {
        IAccount Accounts { get; }

        ITransfer Transfers { get; }

        ITransaction Transactions { get; }

        ICategory Category { get; }
    }
}
