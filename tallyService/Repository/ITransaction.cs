﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public interface ITransaction : IBasicActions<Transactions>
    {
        Task<List<Transactions>> GetTransactionByAccountId(int accountID, bool? isDebit = null);
        Task<List<Transactions>> GetTransactionByDate(DateTime startDate, DateTime endDate, bool? isDebit = null);
    }
}
