﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public interface ITransfer : IBasicActions<Transfers>
    {
        Task<List<Transfers>> GetTransfers();
        Task<List<Transfers>> GetTransfers(int? fromAccountID, int? toAccountID, DateTime? startDate, DateTime? endDate);
        Task<List<Transfers>> GetTransfersByToId(int toAccountID);
        Task<List<Transfers>> GetTransfersByFromId(int fromAccountID);
    }
}
