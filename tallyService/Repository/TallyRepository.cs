﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace tallyService.Repository
{
    public class TallyRepository : ITallyRepository
    {
        public readonly tallyDBContext _context;

        IAccount _accounts;
        ITransfer _transfers;
        ITransaction _transactions;
        ICategory _category;

        public TallyRepository(tallyDBContext context)
        {
            _context = context;
            _context.ChangeTracker.AutoDetectChangesEnabled = false;
        }

        public ITransfer Transfers
        {
            get
            {
                if (_transfers == null)
                {
                    _transfers = new TransferRepo(_context);
                }

                return _transfers;
            }
        } 

        IAccount ITallyRepository.Accounts
        {
            get
            {
                if (_accounts == null)
                {
                    _accounts = new AccountRepo(_context); 
                }

                return _accounts;
            }
        }

        ITransaction ITallyRepository.Transactions
        {
            get
            {
                if (_transactions == null)
                {
                    _transactions = new TransactionRepo(_context);
                }

                return _transactions;
            }
        }

        public ICategory Category
        {
            get
            {
                if (_category == null)
                {
                    _category = new CategoryRepo(_context);
                }

                return _category;
            }
        }
    }
}
