﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public class TransactionRepo : ITransaction
    {
        private readonly tallyDBContext _context;

        public TransactionRepo(tallyDBContext context)
        {
            _context = context;
        }

        public async Task<List<Transactions>> GetTransactionByAccountId(int accountID, bool? isDebit = null)
        {
            if (isDebit == null)
            {
                return await _context.Transactions.AsNoTracking().Where(t => t.AccountId == accountID)
                    .Include(a => a.Account)
                    .Include(a => a.Category)
                    .ToListAsync();
            }
            else
            {
                return await _context.Transactions.AsNoTracking().Where(t => t.AccountId == accountID)
                    .Where(t => t.IsDebit == isDebit.Value)
                    .Include(a => a.Account)
                    .Include(a => a.Category)
                    .ToListAsync();
            }
        }

        public async Task<List<Transactions>> GetTransactionByDate(DateTime startDate, DateTime endDate, bool? isDebit = null)
        {
            if (isDebit == null)
            {
                return await _context.Transactions.AsNoTracking()
                    .Where(t => t.TrasactionDate >= startDate && t.TrasactionDate <= endDate)
                    .Include(a => a.Account)
                    .Include(a => a.Category)
                    .OrderByDescending(t => t.TrasactionDate)
                    .ToListAsync();
            }
            else
            {
                return await _context.Transactions.AsNoTracking()
                    .Where(t => t.TrasactionDate >= startDate && 
                           t.TrasactionDate <= endDate &&
                           t.IsDebit != isDebit) // IsDebit state is saved opposite
                    .Include(a => a.Account)
                    .Include(a => a.Category)
                    .OrderByDescending(t => t.TrasactionDate)
                    .ToListAsync();
            }
        }

        public Task<Transactions> GetRecord(int transactionID, bool trackingEnabled = false)
        {
            return _context.Transactions.Where(t => t.Id == transactionID)
                .Include(a => a.Account)
                .Include(a => a.Category)
                .FirstOrDefaultAsync();
        }

        public Transactions AddRecord(Transactions transactions)
        {
            _context.Transactions.Add(transactions);
            _context.SaveChanges();
            return transactions;
        }

        public bool UpdateRecord(Transactions transactions)
        {
            _context.Transactions.Update(transactions);
            return _context.SaveChanges() > 0;
        }

        public bool DeleteRecord(Transactions transactions)
        {
            var result = _context.Transactions.Remove(transactions);
            return _context.SaveChanges() > 0;
        }
    }
}
