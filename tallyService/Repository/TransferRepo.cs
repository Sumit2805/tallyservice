﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.Repository
{
    public class TransferRepo : ITransfer
    {
        private readonly tallyDBContext _context;

        public TransferRepo(tallyDBContext context)
        {
            _context = context;
        }

        public Transfers AddRecord(Transfers record)
        {
            record.From = _context.Accounts.Where(a => a.Id == record.FromId).FirstOrDefault();
            record.To = _context.Accounts.Where(a => a.Id == record.ToId).FirstOrDefault();
            _context.Transfers.Add(record);
            _context.SaveChanges();
            return record;
        }

        public bool UpdateRecord(Transfers record)
        {
            _context.Transfers.Update(record);
            return _context.SaveChanges() > 0;
        }

        public bool DeleteRecord(Transfers record)
        {
            var result = _context.Transfers.Remove(record);
            return _context.SaveChanges() > 0;
        }

        public async Task<Transfers> GetRecord(int id, bool trackingEnabled = false)
        {
            return await _context.Transfers.Where(a => a.Id == id)
                        .Include(o => o.From)
                        .Include(o => o.To)
                        .FirstOrDefaultAsync();
        }

        public async Task<List<Transfers>> GetTransfers()
        {
            return await _context.Transfers.ToListAsync();
        }

        public async Task<List<Transfers>> GetTransfersByFromId(int fromAccountID)
        {
            return await _context.Transfers.Where(a => a.FromId == fromAccountID)
                .Include(o => o.From)
                .Include(o => o.To)
                .ToListAsync();
        }

        public async Task<List<Transfers>> GetTransfersByToId(int toAccountID)
        {
            return await _context.Transfers.Where(a => a.ToId == toAccountID)
                .Include(o => o.From)
                .Include(o => o.To)
                .ToListAsync();
        }

        public async Task<List<Transfers>> GetTransfers(int? fromAccountID, int? toAccountID, DateTime? startDate, DateTime? endDate)
        {
            if (!startDate.HasValue)
            {
                startDate = new DateTime(1900, 01, 01);
            }

            if (!endDate.HasValue)
            {
                endDate = DateTime.Today;
            }

            var filteredTransfers = await _context.Transfers.Where(t => t.TransferDate >= startDate && t.TransferDate <= endDate)
                .Include(o => o.From)
                .Include(o => o.To)
                .OrderByDescending(t => t.TransferDate)
                .ToListAsync();

            if (toAccountID.HasValue)
            {
                filteredTransfers = filteredTransfers.Where(a => a.ToId == toAccountID).ToList();
            }

            if (fromAccountID.HasValue)
            {
                filteredTransfers = filteredTransfers.Where(a => a.FromId == fromAccountID).ToList();
            }


            return filteredTransfers;
        }
    }
}
