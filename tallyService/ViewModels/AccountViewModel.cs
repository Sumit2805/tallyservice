﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.ViewModels
{
    public class AccountViewModel
    {
        public AccountViewModel()
        {
            //Transactions = new HashSet<Transactions>();
            //TransfersFrom = new HashSet<Transfers>();
            //TransfersTo = new HashSet<Transfers>();
        }
        public int AccountId { get; set; }
        [Required]
        [MinLength(4)]
        public string Name { get; set; }
        public double Initial { get; set; }
        public bool IsClosed { get; set; }
        public string Color { get; set; }
        public bool IsCredit { get; set; }

        //public ICollection<Transactions> Transactions { get; set; }
        //public ICollection<Transfers> TransfersFrom { get; set; }
        //public ICollection<Transfers> TransfersTo { get; set; }
    }
}
