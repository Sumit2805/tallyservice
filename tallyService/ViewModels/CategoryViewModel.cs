﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.ViewModels
{
    public class CategoryViewModel
    {
        public CategoryViewModel()
        {
            Transactions = new HashSet<Transactions>();
        }

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Color { get; set; }
        public bool IsIncome { get; set; }
        public int? Icon { get; set; }

        public ICollection<Transactions> Transactions { get; set; }
    }
}
