﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tallyService.ViewModels
{
    public class LoginRequestViewModel
    {
        public LoginRequestViewModel(string username, string password, string role)
        {
            Username = username;
            Password = password;
            Role = role;
        }

        public string Username { get; }
        public string Password { get; }
        public string Role { get; }
    }
}
