﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tallyService.Models;

namespace tallyService.ViewModels
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Accounts, AccountViewModel>()
                .ForMember(a => a.AccountId, b => b.MapFrom(o => o.Id))
                .ReverseMap();

            CreateMap<Transactions, TransactionsViewModel>()
                .ForMember(a => a.TransactionId, b => b.MapFrom(o => o.Id))
                .ForMember(a => a.TransactionDate, b => b.MapFrom(x => x.TrasactionDate.ToString("yyyy-MM-dd")));
            //.ForMember(a => a.AccountName, b => b.MapFrom(a => a.Account.Name));

            CreateMap<TransactionsViewModel, Transactions>()
                .ForMember(a => a.TrasactionDate, b => b.MapFrom(x => DateTime.Parse(x.TransactionDate)));

            CreateMap<Transfers, TransferViewModel>()
                .ForMember(a => a.TransferDate, b => b.MapFrom(x => x.TransferDate.ToString("yyyy-MM-dd")))
                .ForMember(a => a.FromAccount, b => b.MapFrom(x => x.From.Name))
                .ForMember(a => a.FromId, b => b.MapFrom(x => x.FromId))
                .ForMember(a => a.ToAccount, b => b.MapFrom(x => x.To.Name))
                .ForMember(a => a.ToId, b => b.MapFrom(x => x.ToId))
                .ReverseMap();
        }
    }
}
