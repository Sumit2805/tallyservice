﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tallyService.ViewModels
{
    public class TransactionByDateQM
    {
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
