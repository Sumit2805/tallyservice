﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tallyService.ViewModels
{
    public class TransactionsViewModel
    {
        public int TransactionId { get; set; }
        public double Amount { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string PayeeName { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionMonth { get; set; }
        public bool? IsDebit { get; set; }
        public string Note { get; set; }
        //public bool? IsRepeat { get; set; }
        //public bool? Freq { get; set; }
        //public int? Cycle { get; set; }
        //public bool? IsCleared { get; set; }

        //public Accounts Account { get; set; }
        //public Category Category { get; set; }
    }
}
