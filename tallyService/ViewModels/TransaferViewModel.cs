﻿using System;

namespace tallyService.ViewModels
{
    public class TransferViewModel
    {
        public int Id { get; set; }
        public int? FromId { get; set; }
        public int? ToId { get; set; }
        public string FromAccount { get; set; }
        public string ToAccount { get; set; }
        public double? Amount { get; set; }
        public string TransferDate { get; set; }
        public string Note { get; set; }
        public bool? IsCleared { get; set; }

        //public Accounts From { get; set; }
        //public Accounts To { get; set; }
    }
}
