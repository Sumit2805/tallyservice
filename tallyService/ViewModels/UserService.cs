﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tallyService.ViewModels
{
    public class UserService
    {
        List<LoginRequestViewModel> _users = new List<LoginRequestViewModel>();

        public UserService()
        {
            _users.Add(new LoginRequestViewModel("admin", "admin", "Admin"));
            _users.Add(new LoginRequestViewModel("admin1", "admin1", "Admin"));
            _users.Add(new LoginRequestViewModel("user1", "user1", "User"));
        }

        public string IsValidUser(string username, string password)
        {
            var user = 
                _users.Where(x => x.Username == username && x.Password == password).FirstOrDefault();

            if (user != null)
            {
                return user.Role;
            }
            return null;
        }
    }
}
